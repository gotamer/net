// ping_test.go
package net

import (
	"fmt"
	"testing"
)

func TestSysPing(t *testing.T) {
	out, err := SysPing("bitbucket.com")
	if err != nil {
		t.Errorf("SysPing Error: %g", err)
	} else {
		fmt.Println(out)
	}
}
