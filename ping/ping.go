package net

import (
	"errors"
	"fmt"
	"net"
	"os/exec"
	"regexp"
)

func SysPing(address string) (string, error) {
	out, err := exec.Command("ping", "-c", "1", "-w", "1", "--", address).Output()
	if err != nil {
		errs := fmt.Sprintf("%s", err)
		if errs == "exit status 1" {
			return "", errors.New("Timeout")
		}
		if errs == "exit status 2" {
			return "", errors.New("Unknown host")
		}
		return "", err
	}
	r, err := regexp.Compile(`\d+ bytes from .*`)
	if err != nil {
		return "", err
	}
	line := r.Find(out)
	if line == nil {
		return "", errors.New("Cannot parse ping output")
	}
	return string(line), nil
}

func ping(address string) (*net.IPConn, error) {
	raddr := &net.IPAddr{IP: net.IPv4(127, 0, 0, 1).To4()}
	laddr := &net.IPAddr{IP: net.IPv4(127, 0, 0, 1)}

	ipconn, err := net.DialIP("ip:tcp", laddr, raddr)
	if err != nil {
		fmt.Println("Error: ", err)
	} else {
		fmt.Println(ipconn)
	}
	return ipconn, err
}
