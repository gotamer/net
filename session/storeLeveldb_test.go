// store_test
package session

import (
	"os"
	"testing"
)

var (
	path = os.TempDir() + string(os.PathSeparator) + "test"
)

func TestLevedbStoreSave(t *testing.T) {
	StoreLeveldb(path)
	session := New()
	session.Set("testkey1", "testvalue1")
	session.Set("testkey2", "testvalue2")
	session.Set("testkey3", "testvalue3")
	var err error
	err = session.Save()
	if err != nil {
		t.Error("Session Save to level db error: ", err)
	}
	testsessionid, err = session.Id()
	if err != nil {
		t.Error("Session ID error: ", err)
	}
}
func TestLevedbStoreLoad(t *testing.T) {
	StoreLeveldb(path)
	session, err := Load(testsessionid)
	if err != nil {
		t.Error("Store load error: ", err)
	}
	testone, ok := session.Get("testkey2")
	if ok != true {
		t.Error("Session testkey2 not found")
	}
	if testone != "testvalue2" {
		t.Error("Session key testkey2 != testvalue2")
	}
}
