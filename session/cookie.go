package session

import (
	"net/http"
	"time"
)

const sessionCookieName = "_GoTamer_Session_"

var SessionCookieLifetimeSeconds = 3600 * 24 * 14

func RemoveCookie(w http.ResponseWriter, name string) {
	cookie := &http.Cookie{Name: name, Path: "/", MaxAge: -1}
	http.SetCookie(w, cookie)
}

func SetCookie(w http.ResponseWriter, name, value string) {
	cookie := &http.Cookie{Name: name, Value: value, Path: "/"}
	http.SetCookie(w, cookie)
}

// XXX change seconds to duration?
func SetCookieWithExpiration(w http.ResponseWriter, name, value string, seconds int) {
	cookie := &http.Cookie{
		Name:    name,
		Value:   value,
		Path:    "/",
		MaxAge:  seconds,
		Expires: time.Now().Add(time.Duration(seconds) * time.Second)}
	http.SetCookie(w, cookie)
}

func SetSessionIdCookie(w http.ResponseWriter, s *sessionObject) error {
	if s.modified == false {
		return nil
	}
	sid, err := s.Id()
	if err != nil {
		return err
	}
	SetCookieWithExpiration(w, sessionCookieName, sid, SessionCookieLifetimeSeconds)
	return nil
}
