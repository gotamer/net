// store_test
package session

import (
	"testing"
)

var testsessionid string

func TestSaveStoreMap(t *testing.T) {
	session := New()
	session.Set("test1", "value1")
	session.Set("test2", "value2")
	session.Set("test3", "value3")
	var err error
	testsessionid, err = session.Id()
	if err != nil {
		t.Error("Session ID error: ", err)
	}
	err = session.Save()
	if err != nil {
		t.Error("Store Save error: ", err)
	}
}

func TestLoadStoreMap(t *testing.T) {
	session, err := Load(testsessionid)
	if err != nil {
		t.Error("Store load error: ", err)
	}
	testone, ok := session.Get("test1")
	if ok != true {
		t.Error("Session key test1 not found")
	}
	if testone != "value1" {
		t.Error("Session key test1 not value1")
	}
}
