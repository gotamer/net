// Session for go consists of a way to preserve certain data across
// subsequent accesses in network related applications.
//
// Copyright © 2013 Dennis T Kaplan <http://www.robotamer.com>
//
// Use of this source code is governed by a MIT style license
// that can be found in the README file.
//
// Code can be found at <https://bitbucket.org/gotamer/net/session>
package session

import (
	"crypto/hmac"
	"crypto/md5"
	"encoding/base64"
	"errors"
	"fmt"
	"os"
	"strings"
	"time"
)

const (
	defaultSessionExperation = "48h"
	flashKey                 = "_flash_"
	separator                = "|"
	errKey                   = "_errs_"
)

// Secret is being used as Token salt
//
// TODO: will use this also in the future for enc. and dec. values in the Store.
//
// SessionExpireTime use the same format as defaultSessionExperation
var (
	Secret            = "CHANGE-ME"
	SessionExpireTime string
	registeredStore   Store
	storeRegistered   bool
	cleanupTimer      time.Time
)

// Storing session time and session key
var (
	sessionTimePipe = make(map[time.Time]string)
	// Do not change!!! Internal check!!!
	// Just call CheckSessionExperationLoop() from custom store
	useSessionTimePipe bool
)

type sessionObject struct {
	Sid      string
	Data     map[string]string
	modified bool
}

// Store is an interface for custom session backends.
type Store interface {
	Save(session *sessionObject) (err error)
	Load(sid string) (session *sessionObject, err error)
	Delete(sid string) (err error)
}

func RegisterStore(backend Store) {
	registeredStore = backend
	storeRegistered = true
}

func init() {
	if SessionExpireTime == "" {
		SessionExpireTime = defaultSessionExperation
	}
}

func New() *sessionObject {
	if storeRegistered == false {
		RegisterStore(&storeMap{})
	}
	result := new(sessionObject)
	result.Data = make(map[string]string)
	result.Sid = sessionID()
	// turn on modified flag so the new session will be serialized
	result.modified = true

	// Save session to time pipe?
	if useSessionTimePipe {
		saveSessionTime(result.Sid)
	}
	return result
}

// Saves or updates the session create time to a map
func saveSessionTime(sessionid string) {
	// Scaning pipe to see if there is a entry for this session already
	for key, value := range sessionTimePipe {
		if value == sessionid {
			delete(sessionTimePipe, key)
			break
		}
	}
	sessionTimePipe[time.Now().UTC()] = sessionid
	return
}

func SessionExperationLoop() {
	if useSessionTimePipe {
		return
	}
	useSessionTimePipe = true
	maxege, err := time.ParseDuration(SessionExpireTime)
	if err != nil {
		maxege, _ = time.ParseDuration(defaultSessionExperation)
	}
	go func() {
		for {
			time.Sleep(1 * time.Minute)
			fmt.Println("Running checkSessionExperationLoop")
			timenow := time.Now().UTC()
			for timekey, sessionid := range sessionTimePipe {
				if timenow.Sub(timekey) > maxege {
					delete(sessionTimePipe, timekey)
					err := purge(sessionid)
					fmt.Println("Delete Session failed: ", err)
				}
			}
		}
	}()
}

// Save saves the session to the Store if it has been modified
// use: defer s.Save()
func (s *sessionObject) Save() (err error) {
	if s.modified {
		// This is needed for storeMap (in memory map)
		// glob, xml, json don't store modified
		s.modified = false
		err = registeredStore.Save(s)
		if err != nil {
			s.modified = true
		}
	}
	return err
}

// Restore the session data with the given Sid from the Store
func Load(sid string) (session *sessionObject, err error) {
	session, err = registeredStore.Load(sid)
	if err == nil {
		session.modified = false
	}
	return
}

// This will purge the Session!!!
func (s *sessionObject) Delete() (err error) {
	sid, err := s.Id()
	if err == nil {
		err = purge(sid)
	}
	return
}

func purge(sid string) error {
	return registeredStore.Delete(sid)
}

// Backend returns the session store used to save the session.
func Backend() Store {
	return registeredStore
}

func sessionID() string {
	f, _ := os.Open("/dev/urandom")
	defer f.Close()
	b := make([]byte, 16)
	f.Read(b)
	return fmt.Sprintf("%x", b)
}

func (s *sessionObject) Add(key, val string) (ok bool) {
	if !s.Has(key) {
		s.Data[key] = val
		s.modified = true
		ok = true
	}
	return
}

func (s *sessionObject) Set(key, val string) {
	s.Data[key] = val
	s.modified = true
}

func (s *sessionObject) Get(key string) (val string, ok bool) {
	val, ok = s.Data[key]
	return
}

func (s *sessionObject) Has(key string) (ok bool) {
	_, ok = s.Data[key]
	return
}

func (s *sessionObject) Remove(key string) {
	delete(s.Data, key)
	s.modified = true
}

func (s *sessionObject) AddRecent(key, val string) {
	existing := s.GetList(key)
	updated := make([]string, 1)
	updated[0] = val
	for _, v := range existing {
		if v == val {
			continue
		}
		updated = append(updated, v)
		if len(updated) > 6 {
			break
		}
	}
	s.Set(key, strings.Join(updated, separator))
}

func (s sessionObject) Id() (string, error) {
	if s.Sid == "" {
		return "", errors.New("No session id")
	}
	return s.Sid, nil
}

func (s *sessionObject) GetList(key string) []string {
	existing, ok := s.Get(key)
	if !ok {
		return []string{}
	}
	return strings.Split(existing, separator)
}

func (s *sessionObject) HasFlash() bool {
	flash := s.GetList(flashKey)
	return len(flash) > 0
}

func (s *sessionObject) AddFlash(msg string) {
	s.Push(flashKey, msg)
}

func (s *sessionObject) GetAndResetFlash() []string {
	result := s.GetList(flashKey)
	s.Remove(flashKey)
	return result
}

func (s *sessionObject) AddErrMsg(msg string) {
	s.Push(errKey, msg)
}

func (s *sessionObject) HasErrMsgs() bool {
	msgs := s.GetList(errKey)
	return len(msgs) > 0
}

func (s *sessionObject) GetAndResetErrMsgs() []string {
	result := s.GetList(errKey)
	s.Remove(errKey)
	return result
}

// pushes a value onto a string list
func (s *sessionObject) Push(key, val string) {
	existing, ok := s.Get(key)
	if !ok {
		s.Set(key, val)
		return
	}
	s.Set(key, existing+separator+val)
}

func (s *sessionObject) Token() (string, error) {
	sid, err := s.Id()
	if err != nil {
		return "", err
	}
	h := hmac.New(md5.New, []byte(Secret))
	h.Write([]byte(sid))
	return base64.URLEncoding.EncodeToString(h.Sum(nil)), nil
}

func (s *sessionObject) ValidToken(t string) bool {
	token, _ := s.Token()
	return t == token
}
