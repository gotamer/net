package session

import (
	"bitbucket.org/gotamer/sbs"
	"encoding/gob"
	"github.com/syndtr/goleveldb/leveldb"
	"github.com/syndtr/goleveldb/leveldb/opt"
	"os"
)

const PS = string(os.PathSeparator)

// LeveldbStore stores sessions in a level database.
type LeveldbStore struct {
	Path string
}

func init() {
	gob.Register(LeveldbStore{})
}

// StoreLeveldb() returns a new Level Database Store.
//
// The path argument is the directory where the database will be saved. If empty
// it will use the os.TempDir().
// TODO Load session experation time in to SessionExperationLoop()
//      on start up
func StoreLeveldb(path string) (db *LeveldbStore) {
	if path == "" {
		path = os.TempDir()
	}
	if path[len(path)-1] != os.PathSeparator {
		path += string(os.PathSeparator)
	}
	db = new(LeveldbStore)
	db.Path = path
	//Registering custom store with session
	RegisterStore(db)
	// Turning on sessionTimePipe
	SessionExperationLoop()
	return
}

// Save saves a session to the Store
func (s *LeveldbStore) Save(session *sessionObject) (err error) {
	path := s.Path + PS + "sessions"
	db, err := leveldb.OpenFile(path, &opt.Options{Flag: opt.OFCreateIfMissing})
	if err != nil {
		return
	}
	defer db.Close()
	byteslice, err := sbs.Enc(session)
	if err != nil {
		return
	}
	err = db.Put([]byte(session.Sid), byteslice, &opt.WriteOptions{})
	return
}

// Load restores a session from the Store
func (s *LeveldbStore) Load(sid string) (session *sessionObject, err error) {
	path := s.Path + PS + "sessions"
	db, err := leveldb.OpenFile(path, &opt.Options{Flag: opt.OFCreateIfMissing})
	if err != nil {
		return
	}
	defer db.Close()
	byteslice, err := db.Get([]byte(sid), &opt.ReadOptions{})
	if err != nil {
		return
	}
	so := new(sessionObject)
	se, err := sbs.Dec(so, byteslice)
	session = se.(*sessionObject)
	return
}

// Delete old sessions from the Store
func (s *LeveldbStore) Delete(sid string) (err error) {
	path := s.Path + PS + "sessions"
	db, err := leveldb.OpenFile(path, &opt.Options{Flag: opt.OFCreateIfMissing})
	if err != nil {
		return
	}
	defer db.Close()
	err = db.Delete([]byte(sid), &opt.WriteOptions{})
	return
}
