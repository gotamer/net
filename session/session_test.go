// Copyright (C) 2012 Numerotron Inc.
// Use of this source code is governed by an MIT-style license
// that can be found in the LICENSE file.

package session

import (
	"testing"
)

var store *storeMap

func TestCreate(t *testing.T) {
	s := New()
	if s == nil {
		t.Error("no session created")
	}
}

func TestSessionID(t *testing.T) {
	s := New()
	sid, _ := s.Id()
	if len(sid) == 0 {
		t.Errorf("session id empty")
	}
}

func TestRemove(t *testing.T) {
	s := New()
	s.Set("x", "asdf")
	v, ok := s.Get("x")
	if !ok {
		t.Errorf("key x not in session")
	}
	if v != "asdf" {
		t.Errorf("get failed")
	}

	ok = s.Add("x", "asdf2")
	if ok {
		t.Errorf("Add should not replace key x")
	}

	s.Remove("x")
	ok = s.Has("x")
	if ok {
		t.Errorf("remove failed, x still exists")
	}

	v, _ = s.Get("x")
	if v != "" {
		t.Errorf("remove failed, x still = %s", v)
	}
}

func TestAddRecent(t *testing.T) {
	s := New()
	list := s.GetList("recent")
	if len(list) != 0 {
		t.Errorf("list should be empty")
	}
	s.AddRecent("recent", "asdf")
	list = s.GetList("recent")
	if len(list) != 1 {
		t.Errorf("list should have 1 elt")
	}
	s.AddRecent("recent", "qwer")
	list = s.GetList("recent")
	if len(list) != 2 {
		t.Errorf("list should have 2 elts")
	}
	if list[0] != "qwer" {
		t.Errorf("first elt should be 'qwer'")
	}
	s.AddRecent("recent", "asdf")
	list = s.GetList("recent")
	if len(list) != 2 {
		t.Errorf("list should have 2 elts")
	}
	if list[0] != "asdf" {
		t.Errorf("first elt should be 'qwer'")
	}
}

func TestAddFlash(t *testing.T) {
	s := New()
	if s.HasFlash() {
		t.Errorf("new session should not have flash")
	}
	s.AddFlash("message")
	if s.HasFlash() == false {
		t.Errorf("expected session to have flash")
	}
	flashList := s.GetAndResetFlash()
	if len(flashList) != 1 {
		t.Errorf("expected flash list to be len 1, not %d", len(flashList))
	}
	if s.HasFlash() == true {
		t.Errorf("after get and reset, expected no flash")
	}
	if flashList[0] != "message" {
		t.Errorf("flash msg mismatch, expected 'message', got %q", flashList[0])
	}

	s.AddFlash("msg 1")
	s.AddFlash("msg 2")
	flashList = s.GetAndResetFlash()
	if len(flashList) != 2 {
		t.Errorf("expected flash list to be len 1, not %d", len(flashList))
	}
	if s.HasFlash() == true {
		t.Errorf("after get and reset, expected no flash")
	}
	if flashList[0] != "msg 1" {
		t.Errorf("flash msg mismatch, expected 'msg 1', got %q", flashList[0])
	}
	if flashList[1] != "msg 2" {
		t.Errorf("flash msg mismatch, expected 'msg 2', got %q", flashList[1])
	}
}
