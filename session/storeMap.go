package session

import (
	"errors"
	"time"
)

var (
	sessions = make(map[string]storeMap)
)

type storeMap struct {
	session *sessionObject
	t       time.Time
}

func (store *storeMap) Save(session *sessionObject) (err error) {
	store.session = session
	store.t = time.Now().UTC()
	key, err := session.Id()
	if err != nil {
		return
	}
	sessions[key] = *store
	cleanSessionMap()
	return
}

func (store *storeMap) Load(sid string) (session *sessionObject, err error) {
	emptyObject := new(storeMap)
	if sessions[sid] != *emptyObject {
		s := sessions[sid]
		store.session = s.session
		return store.session, err
	} else {
		err = errors.New("Session not found in Store")
	}
	return
}

func (store *storeMap) Delete(sid string) (err error) {
	delete(sessions, sid)
	return
}

// Removes old sessions from the Store based on the time
// duration given at SessionExpireTime
func cleanSessionMap() {
	if SessionExpireTime == "" {
		SessionExpireTime = defaultSessionExperation
	}
	timenow := time.Now().UTC()
	skipTimer, _ := time.ParseDuration("10m")
	if timenow.Sub(cleanupTimer) < skipTimer {
		return
	}
	maxege, err := time.ParseDuration(SessionExpireTime)
	if err != nil {
		maxege, _ = time.ParseDuration(defaultSessionExperation)
	}
	for key, store := range sessions {
		if timenow.Sub(store.t) > maxege {
			delete(sessions, key)
		}
	}
	cleanupTimer = timenow
}
